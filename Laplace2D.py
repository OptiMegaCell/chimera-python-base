from mpl_toolkits.mplot3d import Axes3D
import numpy
from matplotlib import pyplot, cm
import time, sys
domain = 2 #size
nx = 31 #meshsize in x
ny = 31 #meshsize in y
dx = float(float(domain) / float(nx - 1)) #distance between grid points
dy = float(float(domain) / float(ny - 1)) #distance between grid points

nt = 800 #number of timesteps

c = 1 #wavespeed

x = numpy.linspace(0, 2, nx)
y = numpy.linspace(0, 1, ny)

p = numpy.zeros((ny, nx)) #sets everything to 1


print(dx)
p[:, 0] = 0  # p = 0 @ x = 0
p[:, -1] = y  # p = y @ x = 2
p[0, :] = p[1, :]  # dp/dy = 0 @ y = 0
p[-1, :] = p[-2, :]  # dp/dy = 0 @ y = 1

fig = pyplot.figure(figsize=(11, 7), dpi=100)
ax = fig.gca(projection='3d')
ax.grid(False)
X, Y = numpy.meshgrid(x, y)
surf = ax.plot_surface(X, Y, p[:], linewidth=0, rstride=1, cstride=1, cmap=cm.YlGnBu_r)
ax.set_xlabel('$x$')
ax.set_ylabel('$y$')
ax.set_xlim(0, 2)
ax.set_ylim(0, 1)
#pyplot.show()

#fig = pyplot.figure(figsize=(11, 7), dpi=100)
#ax = fig.gca(projection='3d')

for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
    pn = p.copy() ##copy the existing values of u into un

    row, col = p.shape #simply gets the size of u for loop iterations
    for j in range(1, row - 1):
        for i in range(1, col - 1):
            p[j, i] = (dy**2 * (pn[j, i+1] + pn[j, i-1]) + dx**2 * (pn[j+1, i] + pn[j-1, i])) / (2 * (dx**2 + dy**2))

    p[:, 0] = 0  # p = 0 @ x = 0
    p[:, -1] = y  # p = y @ x = 2
    p[0, :] = p[1, :]  # dp/dy = 0 @ y = 0
    p[-1, :] = p[-2, :]  # dp/dy = 0 @ y = 1

    surf.remove()
    surf = ax.plot_surface(X, Y, p[:], rstride=1, cstride=1, linewidth=0, cmap=cm.YlGnBu_r)
    ax.set_xlim(0, 2)
    ax.set_ylim(0, 1)
    pyplot.draw()
    pyplot.pause(0.00001)
