from mpl_toolkits.mplot3d import Axes3D
import numpy
from matplotlib import pyplot, cm
import time, sys
domain = 3 #size
nx = 41 #meshsize in x
ny = 41 #meshsize in y
dx = float(float(domain) / float(nx - 1)) #distance between grid points
dy = float(float(domain) / float(ny - 1)) #distance between grid points

sigma = .04
nu = 0.07 #viscosity
nt = 240 #number of timesteps
dt = sigma*dx*dy / nu #amount of time each timestep covers - delta t
c = 1 #wavespeed

x = numpy.linspace(0, 3, nx)
y = numpy.linspace(0, 3, ny)

u = numpy.ones((ny, nx)) #sets everything to 1
un = numpy.ones((ny, nx)) #sets everything to 1
v = numpy.ones((ny, nx)) #sets everything to 1
vn = numpy.ones((ny, nx)) #sets everything to 1
comb = numpy.ones((ny, nx))

print(dx)

u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions
v[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

fig = pyplot.figure(figsize=(11, 7), dpi=100)
ax = fig.gca(projection='3d')
ax.grid(False)
X, Y = numpy.meshgrid(x, y)
surf = ax.plot_surface(X, Y, u[:], rstride=1, cstride=1, linewidth=0, cmap=cm.YlGnBu_r)
ax.set_xlabel('$x$')
ax.set_ylabel('$y$');
ax.set_zlim3d(1, 2.0)
#pyplot.show()

#fig = pyplot.figure(figsize=(11, 7), dpi=100)
#ax = fig.gca(projection='3d')

for n in range(nt):  #loop for values of n from 0 to nt, so it will run nt times
    un = u.copy() ##copy the existing values of u into un
    vn = v.copy() ##copy the existing values of v into vn
    row, col = u.shape #simply gets the size of u for loop iterations

    for j in range(1, row - 1):
        for i in range(1, col - 1):
            u[j, i] = un[j, i] - un[j, i] * dt / dx * (un[j, i] - un[j, i-1]) - vn[j, i] * dt / dy * (un[j, i] - un[j-1, i]) + nu * (dt / dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * (dt / dy**2) * (un[j+1, i] - 2*un[j, i] + un[j+1, i])
            v[j, i] = vn[j, i] - un[j, i] * dt / dx * (vn[j, i] - vn[j, i-1]) - vn[j, i] * dt / dy * (vn[j, i] - vn[j-1, i]) + nu * (dt / dx**2) * (vn[j, i+1] - 2*vn[j, i] + vn[j, i-1]) + nu * (dt / dy**2) * (vn[j+1, i] - 2*vn[j, i] + vn[j+1, i])

    u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
    u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
    u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
    u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

    v[0, :] = 1 #sets the whole of [0, ] to 1 for all i
    v[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
    v[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
    v[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j



    surf.remove()
    surf = ax.plot_surface(X, Y, u[:], rstride=1, cstride=1, linewidth=0, cmap=cm.YlGnBu_r)
    ax.set_zlim3d(1, 2.0)
    pyplot.draw()
    pyplot.pause(0.00001)
