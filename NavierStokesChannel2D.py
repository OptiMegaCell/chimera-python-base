from mpl_toolkits.mplot3d import Axes3D
import numpy
numpy.set_printoptions(threshold=numpy.nan)
from matplotlib import pyplot, cm
import time, sys
domain = 2 #size
nx = 41 #meshsize in x
ny = 41 #meshsize in y
dx = float(float(domain) / float(nx - 1)) #distance between grid points
dy = float(float(domain) / float(ny - 1)) #distance between grid points

nt = 500 #number of timesteps
nit = 50
nu = 0.1 #viscosity
c = 1 #wavespeed
rho = 1 #density
dt = .01
x = numpy.linspace(0, 2, nx)
y = numpy.linspace(0, 2, ny)
F = 1 #constant force
p = numpy.zeros((ny, nx)) #sets everything to 1
pn = numpy.ones((ny, nx))
u = numpy.zeros((ny, nx)) #sets everything to 1
un = numpy.zeros((ny, nx))
v = numpy.zeros((ny, nx)) #sets everything to 1
vn = numpy.zeros((ny, nx))
b  = numpy.zeros((ny, nx))
X, Y = numpy.meshgrid(x, y)
print(dx)


#b[int(ny / 4), int(nx / 4)]  = 100 #inital b
#b[int(3 * ny / 4), int(3 * nx / 4)] = -100 #inital b

fig = pyplot.figure(figsize=(11, 7), dpi=100)


#pyplot.show()

#fig = pyplot.figure(figsize=(11, 7), dpi=100)
#ax = fig.gca(projection='3d')

for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
    row, col = b.shape #simply gets the size of u for loop iterations
    for j in range(1, row - 1):
        for i in range(1, col - 1):
            b[j, i] = rho * ( (1 / dt * (((u[j, i+1] - u[j, i-1])/ (2 * dx)) + (v[j+1, i] - v[j-1, i])/ (2 * dy))) - ((u[j, i+1] - u[j, i-1])/ (2 * dx))**2 - 2 * ((v[j, i+1] - v[j, i-1])/ (2 * dx) * (u[j+1, i] - u[j-1, i])/ (2 * dy)) - ((v[j+1, i] - v[j-1, i])/ (2 * dy))**2 )

        b[j, row - 1 ] = rho * ( (1 / dt * (((u[j, 0] - u[j, row-2])/ (2 * dx)) + (v[j+1, row-1] - v[j-1, row-1])/ (2 * dy))) - ((u[j, 0] - u[j, row-2])/ (2 * dx))**2 - 2 * ((v[j, 0] - v[j, row-2])/ (2 * dx) * (u[j+1, row-1] - u[j-1, row-1])/ (2 * dy)) - ((v[j+1, row-1] - v[j-1, row-1])/ (2 * dy))**2 )
        b[j, 0] = rho * ( (1 / dt * (((u[j, 1] - u[j, row-1])/ (2 * dx)) + (v[j+1, 0] - v[j-1, 0])/ (2 * dy))) - ((u[j, 1] - u[j, row-1])/ (2 * dx))**2 - 2 * ((v[j, 1] - v[j, row-1])/ (2 * dx) * (u[j+1, 0] - u[j-1, 0])/ (2 * dy)) - ((v[j+1, 0] - v[j-1, 0])/ (2 * dy))**2 )

    for l in range(nit + 1):
        pn = p.copy() ##copy the existing values of u into un
        row, col = p.shape
        for j in range(1, row - 1):
            for i in range(1, col - 1):
                p[j, i] = (((pn[j, i+1] + pn[j, i-1]) * dy**2 + (pn[j+1 ,i] + pn[j-1, i]) * dx**2) / (2 * (dx**2 + dy**2))) - (dx**2 * dy**2 * b[j, i]) / (2 * (dx**2 + dy**2))

            p[j, row - 1] = (((pn[j, 0] + pn[j, row-2]) * dy**2 + (pn[j+1 , row -1] + pn[j-1, row -1]) * dx**2) / (2 * (dx**2 + dy**2))) - (dx**2 * dy**2 * b[j, row -1]) / (2 * (dx**2 + dy**2))
            p[j, 0] = (((pn[j, 1] + pn[j, row-1]) * dy**2 + (pn[j+1 , 0] + pn[j-1, 0]) * dx**2) / (2 * (dx**2 + dy**2))) - (dx**2 * dy**2 * b[j, 0]) / (2 * (dx**2 + dy**2))

        p[-1, :] = p[-2, :] # dp/dy = 0 at y = 2
        p[0, :] = p[1, :]   # dp/dy = 0 at y = 0

    un = u.copy() ##copy the existing values of u into un
    vn = v.copy() ##copy the existing values of v into vn
    row, col = u.shape #simply gets the size of u for loop iterations

    for j in range(1, row - 1):
        for i in range(1, col - 1):
            u[j, i] = un[j, i] - un[j, i] * dt / dx * (un[j, i] - un[j, i-1]) - vn[j, i] * dt / dy * (un[j, i] - un[j-1, i]) - (dt / (rho * 2 * dx)) * (p[j, i+1] - p[j, i-1]) + nu * (dt / dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * (dt / dy**2) * (un[j+1, i] - 2*un[j, i] + un[j-1, i]) + F * dt
            v[j, i] = vn[j, i] - un[j, i] * dt / dx * (vn[j, i] - vn[j, i-1]) - vn[j, i] * dt / dy * (vn[j, i] - vn[j-1, i]) - (dt / (rho * 2 * dy)) * (p[j+1, i] - p[j-1, i]) + nu * (dt / dx**2) * (vn[j, i+1] - 2*vn[j, i] + vn[j, i-1]) + nu * (dt / dy**2) * (vn[j+1, i] - 2*vn[j, i] + vn[j-1, i])

        u[j, row - 1] = un[j, row - 1] - un[j, row - 1] * dt / dx * (un[j, row - 1] - un[j, row - 2]) - vn[j, row - 1] * dt / dy * (un[j, row - 1] - un[j-1, row - 1]) - (dt / (rho * 2 * dx)) * (p[j, 0] - p[j, row - 2]) + nu * (dt / dx**2) * (un[j, 0] - 2*un[j, row - 1] + un[j, row - 2]) + nu * (dt / dy**2) * (un[j+1, row - 1] - 2*un[j, row - 1] + un[j-1, row - 1]) + F * dt
        u[j, 0] = un[j, 0] - un[j, 0] * dt / dx * (un[j, 0] - un[j, row - 1]) - vn[j, 0] * dt / dy * (un[j, 0] - un[j-1, 0]) - (dt / (rho * 2 * dx)) * (p[j, 1] - p[j, row - 1]) + nu * (dt / dx**2) * (un[j, 1] - 2*un[j, 0] + un[j, row - 1]) + nu * (dt / dy**2) * (un[j+1, 0] - 2*un[j, 0] + un[j-1, 0]) + F * dt

        v[j, row - 1] = vn[j, row - 1] - un[j, row - 1] * dt / dx * (vn[j, row - 1] - vn[j, row - 2]) - vn[j, row - 1] * dt / dy * (vn[j, row - 1] - vn[j-1, row - 1]) - (dt / (rho * 2 * dx)) * (p[j+1, row -1] - p[j-1, row - 1]) + nu * (dt / dx**2) * (vn[j, 0] - 2*vn[j, row - 1] + vn[j, row - 2]) + nu * (dt / dy**2) * (vn[j+1, row - 1] - 2*vn[j, row - 1] + vn[j-1, row - 1])
        v[j, 0] = vn[j, 0] - un[j, 0] * dt / dx * (vn[j, 0] - vn[j, row - 1]) - vn[j, 0] * dt / dy * (vn[j, 0] - vn[j-1, 0]) - (dt / (rho * 2 * dx)) * (p[j+1, 0] - p[j-1, 0]) + nu * (dt / dx**2) * (vn[j, 1] - 2*vn[j, 0] + vn[j, row - 1]) + nu * (dt / dy**2) * (vn[j+1, 0] - 2*vn[j, 0] + vn[j-1, 0])

    u[0, :] = 0 #sets the whole of [0, ] to 1 for all i
    u[-1, :] = 0 #sets the whole of [last element, i] to 1 for all i
    v[0, :] = 0 #sets the whole of [0, ] to 1 for all i
    v[-1, :] = 0 #sets the whole of [last element, i] to 1 for all i


    try:
        pyplot.clf()
        pyplot.quiver(X[::3, ::3], Y[::3, ::3], u[::3, ::3], v[::3, ::3])
        pyplot.pause(0.00001)
    except ValueError:
        pass

#pyplot.show()


#pyplot.pause(0.00001)
