import numpy

from numba import cuda
from numba import *
import time, sys
from datetime import datetime


def calculateLinearConvection(value, lastvaluei, lastvaluej, c, dt, dx, dy):
    return value - c * dt / dx * (value - lastvaluei) - c * dt / dy * (value - lastvaluej)

def calculateNonLinearConvectionU(uvalue, vvalue, lastvaluei, lastvaluej, dt, dx, dy):
    return uvalue - uvalue * dt / dx * (uvalue - lastvaluei) - vvalue * dt / dy * (uvalue - lastvaluej)

def calculateNonLinearConvectionV(uvalue, vvalue, lastvaluei, lastvaluej, dt, dx, dy):
    return vvalue - uvalue * dt / dx * (vvalue - lastvaluei) - vvalue * dt / dy * (vvalue - lastvaluej)

def calculateDiffusion(value, lastvaluei, lastvaluej, nextvaluei, nextvaluej, dt, dx, dy, nu):
    return value + nu * dt / (dx**2) * (nextvaluei - 2*value + lastvaluei) + nu * dt / (dx**2) * (nextvaluej - 2*value + lastvaluej)
    #u[j, i] = un[j, i] + nu * dt / (dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * dt / (dx**2) * (un[j+1, i] - 2*un[j, i] + un[j-1, i])

calculateLinearConvection_gpu = cuda.jit(device=True)(calculateLinearConvection)
calculateNonLinearConvectionU_gpu = cuda.jit(device=True)(calculateNonLinearConvectionU)
calculateNonLinearConvectionV_gpu = cuda.jit(device=True)(calculateNonLinearConvectionV)
calculateDiffusion_gpu = cuda.jit(device=True)(calculateDiffusion)

@cuda.jit
def LinearConvection(domain, nx, ny, sigma, nt, c, u, un):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    dt = sigma*dx #amount of time each timestep covers - delta t

    u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

    startX, startY = cuda.grid(2)
    gridX = cuda.gridDim.x * cuda.blockDim.x
    gridY = cuda.gridDim.y * cuda.blockDim.y

    for n in range(nt+1):  #loop for values of n from 0 to nt, so it will run nt times
        #un = u.copy() ##copy the existing values of u into un
        row, col = u.shape #simply gets the size of u for loop iterations

        for j in range(startX, row, gridX):
            for i in range(startY, col, gridY):
                un[j, i] = u[j, i]
        
        for j in range(startX, row, gridX):
            for i in range(startY, col, gridY):
                #u[j, i] = un[j, i] - c * dt / dx * (un[j, i] - un[j, i-1]) - c * dt / dy * (un[j, i] - un[j-1, i])
                u[j, i] = calculateLinearConvection_gpu(un[j, i], un[j, i-1], un[j-1, i], c, dt, dx, dy)
        
        u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

@cuda.jit
def NonLinearConvection(domain, nx, ny, sigma, nt, u, un, v, vn):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    dt = sigma*dx #amount of time each timestep covers - delta t

    u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions
    v[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

    startX, startY = cuda.grid(2)
    gridX = cuda.gridDim.x * cuda.blockDim.x
    gridY = cuda.gridDim.y * cuda.blockDim.y

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        #un = u.copy() ##copy the existing values of u into un
        #vn = v.copy() ##copy the existing values of v into vn
        row, col = u.shape #simply gets the size of u for loop iterations

        for j in range(startX, row, gridX):
            for i in range(startY, col, gridY):
                un[j, i] = u[j, i]
        
        for j in range(startX, row, gridX):
            for i in range(startY, col, gridY):
                vn[j, i] = v[j, i]
        
        for j in range(startX, row, gridX):
            for i in range(startY, col, gridY):
                u[j, i] = calculateNonLinearConvectionU_gpu(un[j, i], vn[j, i], un[j, i-1], un[j-1, i], dt, dx, dy)
                v[j, i] = calculateNonLinearConvectionV_gpu(un[j, i], vn[j, i], vn[j, i-1], vn[j-1, i], dt, dx, dy)

        u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

        v[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        v[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        v[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        v[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

@cuda.jit
def Diffusion(domain, nx, ny, sigma, nt,  nu, u, un):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    dt = sigma * dx**2 / nu #amount of time each timestep covers - delta t
    #u = numpy.ones((ny, nx)) #sets everything to 1
    #un = numpy.ones((ny, nx)) #sets everything to 1

    u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

    startX, startY = cuda.grid(2)
    gridX = cuda.gridDim.x * cuda.blockDim.x
    gridY = cuda.gridDim.y * cuda.blockDim.y

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        #un = u.copy() ##copy the existing values of u into un
        row, col = u.shape #simply gets the size of u for loop iterations

        for j in range(startX, row, gridX):
            for i in range(startY, col, gridY):
                un[j, i] = u[j, i]

        for j in range(startX, row-1, gridX):
            for i in range(startY, col-1 ,gridY):
                u[j, i] = calculateDiffusion_gpu(un[j, i], un[j, i-1], un[j-1, i], un[j, i+1], un[j+1, i], dt, dx, dy, nu)
                #u[j, i] = un[j, i] + nu * dt / (dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * dt / (dx**2) * (un[j+1, i] - 2*un[j, i] + un[j-1, i])

        u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j


t1 = datetime.now()
blockdim = (32, 8)
griddim = (32,16)

print("Linear Convection Running")

u_image = numpy.ones((61, 61))
un_image = numpy.ones((61, 61))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
LinearConvection[griddim, blockdim](2, 61, 61, 0.2, 1, 1, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((61, 61))
un_image = numpy.ones((61, 61))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
LinearConvection[griddim, blockdim](2, 61, 61, 0.2, 4000, 1, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((121, 121))
un_image = numpy.ones((121, 121))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
LinearConvection[griddim, blockdim](2, 121, 121, 0.2, 8000, 1, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((241, 241))
un_image = numpy.ones((241, 241))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
LinearConvection[griddim, blockdim](2, 241, 241, 0.2, 8000, 1, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((241, 241))
un_image = numpy.ones((241, 241))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
LinearConvection[griddim, blockdim](2, 241, 241, 0.2, 16000, 2, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((2441, 2441))
un_image = numpy.ones((2441, 2441))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
LinearConvection[griddim, blockdim](2, 241, 241, 0.2, 24000, 0.5, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

print("Linear Convection Done\n")
print("Non-Linear Convection Running")

u_image = numpy.ones((51, 51))
un_image = numpy.ones((51, 51))
v_image = numpy.ones((51, 51))
vn_image = numpy.ones((51, 51))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
vd_image = cuda.to_device(v_image)
vnd_image = cuda.to_device(vn_image)
NonLinearConvection[griddim, blockdim](2, 51, 51, 0.2, 1, ud_image, und_image, vd_image, vnd_image)
ud_image.to_host()
und_image.to_host()
vd_image.to_host()
vnd_image.to_host()

u_image = numpy.ones((51, 51))
un_image = numpy.ones((51, 51))
v_image = numpy.ones((51, 51))
vn_image = numpy.ones((51, 51))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
vd_image = cuda.to_device(v_image)
vnd_image = cuda.to_device(vn_image)
NonLinearConvection[griddim, blockdim](2, 51, 51, 0.2, 4000, ud_image, und_image, vd_image, vnd_image)
ud_image.to_host()
und_image.to_host()
vd_image.to_host()
vnd_image.to_host()

u_image = numpy.ones((51, 51))
un_image = numpy.ones((51, 51))
v_image = numpy.ones((51, 51))
vn_image = numpy.ones((51, 51))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
vd_image = cuda.to_device(v_image)
vnd_image = cuda.to_device(vn_image)
NonLinearConvection[griddim, blockdim](2, 101, 101, 0.2, 4000, ud_image, und_image, vd_image, vnd_image)
ud_image.to_host()
und_image.to_host()
vd_image.to_host()
vnd_image.to_host()

u_image = numpy.ones((51, 51))
un_image = numpy.ones((51, 51))
v_image = numpy.ones((51, 51))
vn_image = numpy.ones((51, 51))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
vd_image = cuda.to_device(v_image)
vnd_image = cuda.to_device(vn_image)
NonLinearConvection[griddim, blockdim](2, 151, 151, 0.2, 8000, ud_image, und_image, vd_image, vnd_image)
ud_image.to_host()
und_image.to_host()
vd_image.to_host()
vnd_image.to_host()

u_image = numpy.ones((51, 51))
un_image = numpy.ones((51, 51))
v_image = numpy.ones((51, 51))
vn_image = numpy.ones((51, 51))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
vd_image = cuda.to_device(v_image)
vnd_image = cuda.to_device(vn_image)
NonLinearConvection[griddim, blockdim](2, 201, 201, 0.2, 8000, ud_image, und_image, vd_image, vnd_image)
ud_image.to_host()
und_image.to_host()
vd_image.to_host()
vnd_image.to_host()

print("Non-Linear Convection Done\n")

print("Diffusion Running")
u_image = numpy.ones((121, 121))
un_image = numpy.ones((121, 121))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
Diffusion[griddim, blockdim](2, 121, 121, 0.1, 1, 0.000111, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((121, 121))
un_image = numpy.ones((121, 121))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
Diffusion[griddim, blockdim](2, 121, 121, 0.1, 8000, 0.000111, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((121, 121))
un_image = numpy.ones((121, 121))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
Diffusion[griddim, blockdim](2, 121, 121, 0.25, 8000, 0.00001172, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((241, 241))
un_image = numpy.ones((241, 241))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
Diffusion[griddim, blockdim](2, 241, 241, 0.5, 16000, 0.0002165, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((241, 241))
un_image = numpy.ones((241, 241))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
Diffusion[griddim, blockdim](2, 241, 241, 0.5, 16000, 0.000019, ud_image, und_image)
ud_image.to_host()
und_image.to_host()

u_image = numpy.ones((241, 241))
un_image = numpy.ones((241, 241))
ud_image = cuda.to_device(u_image)
und_image = cuda.to_device(un_image)
Diffusion[griddim, blockdim](2, 241, 241, 0.5, 16000, 1.43e-7, ud_image, und_image)
ud_image.to_host()
und_image.to_host()
print("Diffusion Done\n")

t2 = datetime.now()

timediff = t2 - t1

print("Total time: " + str(timediff.total_seconds() * 1000) + " ms")