import numpy

from numba import jit
import time, sys
from datetime import datetime

@jit(nopython=True)
def LinearConvection(domain, nx, ny, sigma, nt, c):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    dt = sigma*dx #amount of time each timestep covers - delta t
    u = numpy.ones((ny, nx)) #sets everything to 1
    un = numpy.ones((ny, nx)) #sets everything to 1

    u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        un = u.copy() ##copy the existing values of u into un
        row, col = u.shape #simply gets the size of u for loop iterations
        for j in range(1, row):
            for i in range(1, col):
                u[j, i] = un[j, i] - c * dt / dx * (un[j, i] - un[j, i-1]) - c * dt / dy * (un[j, i] - un[j-1, i])
        u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

@jit(nopython=True)
def NonLinearConvection(domain, nx, ny, sigma, nt):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    dt = sigma*dx #amount of time each timestep covers - delta t
    u = numpy.ones((ny, nx)) #sets everything to 1
    un = numpy.ones((ny, nx)) #sets everything to 1
    v = numpy.ones((ny, nx)) #sets everything to 1
    vn = numpy.ones((ny, nx)) #sets everything to 1

    u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions
    v[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        un = u.copy() ##copy the existing values of u into un
        vn = v.copy() ##copy the existing values of v into vn
        row, col = u.shape #simply gets the size of u for loop iterations
        for j in range(1, row):
            for i in range(1, col):
                u[j, i] = un[j, i] - un[j, i] * dt / dx * (un[j, i] - un[j, i-1]) - vn[j, i] * dt / dy * (un[j, i] - un[j-1, i])
                v[j, i] = vn[j, i] - un[j, i] * dt / dx * (vn[j, i] - vn[j, i-1]) - vn[j, i] * dt / dy * (vn[j, i] - vn[j-1, i])

        u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

        v[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        v[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        v[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        v[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

@jit(nopython=True)
def Diffusion(domain, nx, ny, sigma, nt, nu):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    dt = sigma * dx**2 / nu #amount of time each timestep covers - delta t
    u = numpy.ones((ny, nx)) #sets everything to 1
    un = numpy.ones((ny, nx)) #sets everything to 1

    u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

    for n in range(nt+1):  #loop for values of n from 0 to nt, so it will run nt times
        un = u.copy() ##copy the existing values of u into un
        row, col = u.shape #simply gets the size of u for loop iterations
        for j in range(1, row-1):
            for i in range(1, col-1):
                u[j, i] = un[j, i] + nu * dt / (dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * dt / (dx**2) * (un[j+1, i] - 2*un[j, i] + un[j-1, i])

        u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

@jit(nopython=True)
def Burgers(domain, nx, ny, sigma, nt, nu):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    dt =  sigma*dx**2 / nu #amount of time each timestep covers - delta t
    u = numpy.ones((ny, nx)) #sets everything to 1
    un = numpy.ones((ny, nx)) #sets everything to 1
    v = numpy.ones((ny, nx)) #sets everything to 1
    vn = numpy.ones((ny, nx)) #sets everything to 1

    u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions
    v[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

    for n in range(nt):  #loop for values of n from 0 to nt, so it will run nt times
        un = u.copy() ##copy the existing values of u into un
        vn = v.copy() ##copy the existing values of v into vn
        row, col = u.shape #simply gets the size of u for loop iterations

        for j in range(1, row - 1):
            for i in range(1, col - 1):
                u[j, i] = un[j, i] - un[j, i] * dt / dx * (un[j, i] - un[j, i-1]) - vn[j, i] * dt / dy * (un[j, i] - un[j-1, i]) + nu * (dt / dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * (dt / dy**2) * (un[j+1, i] - 2*un[j, i] + un[j+1, i])
                v[j, i] = vn[j, i] - un[j, i] * dt / dx * (vn[j, i] - vn[j, i-1]) - vn[j, i] * dt / dy * (vn[j, i] - vn[j-1, i]) + nu * (dt / dx**2) * (vn[j, i+1] - 2*vn[j, i] + vn[j, i-1]) + nu * (dt / dy**2) * (vn[j+1, i] - 2*vn[j, i] + vn[j+1, i])

        u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

        v[0, :] = 1 #sets the whole of [0, ] to 1 for all i
        v[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        v[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
        v[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

@jit(nopython=True)
def Laplace(domain, nx, ny, nt):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    p = numpy.zeros((ny, nx)) #sets everything to 0
    y = numpy.linspace(0, 1, ny)
    p[:, 0] = 0  # p = 0 @ x = 0
    p[:, -1] = y  # p = y @ x = 2
    p[0, :] = p[1, :]  # dp/dy = 0 @ y = 0
    p[-1, :] = p[-2, :]  # dp/dy = 0 @ y = 1

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        pn = p.copy() ##copy the existing values of u into un
        row, col = p.shape #simply gets the size of u for loop iterations
        for j in range(1, row - 1):
            for i in range(1, col - 1):
                p[j, i] = (dy**2 * (pn[j, i+1] + pn[j, i-1]) + dx**2 * (pn[j+1, i] + pn[j-1, i])) / (2 * (dx**2 + dy**2))

        p[:, 0] = 0  # p = 0 @ x = 0
        p[:, -1] = y  # p = y @ x = 2
        p[0, :] = p[1, :]  # dp/dy = 0 @ y = 0
        p[-1, :] = p[-2, :]  # dp/dy = 0 @ y = 1

@jit(nopython=True)
def Poisson(domain, nx, ny, nt):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    p = numpy.zeros((ny, nx)) #sets everything to 0
    b  = numpy.zeros((ny, nx)) #sets everything to 0

    b[int(ny / 4), int(nx / 4)]  = 100 #inital b
    b[int(3 * ny / 4), int(3 * nx / 4)] = -100 #inital b

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        pn = p.copy() ##copy the existing values of u into un

        row, col = p.shape #simply gets the size of u for loop iterations
        for j in range(1, row - 1):
            for i in range(1, col - 1):
                p[j, i] = ((dy**2 * (pn[j, i+1] + pn[j, i-1]) + dx**2 * (pn[j+1, i] + pn[j-1, i])) - b[j, i] * dx**2 * dy**2) / (2 * (dx**2 + dy**2))

        p[0, :] = 0 #sets the whole of [0, ] to 1 for all i
        p[ny-1, :] = 0 #sets the whole of [last element, i] to 1 for all i
        p[:, 0] = 0 #sets the whole of [j, 0] to 1 for all j
        p[:, nx-1] = 0 #sets the whole of [j, last element] to 1 for all j

@jit(nopython=True)
def NavierCavity(domain, nx, ny, nt, nit, nu, rho, dt):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    p = numpy.zeros((ny, nx)) #sets everything to 0
    b  = numpy.zeros((ny, nx)) #sets everything to 0
    u  = numpy.zeros((ny, nx)) #sets everything to 0
    v  = numpy.zeros((ny, nx)) #sets everything to 0

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        row, col = b.shape #simply gets the size of u for loop iterations
        for j in range(1, row - 1):
            for i in range(1, col - 1):
                b[j, i] = rho * ( (1 / dt * (((u[j, i+1] - u[j, i-1])/ (2 * dx)) + (v[j+1, i] - v[j-1, i])/ (2 * dy))) - ((u[j, i+1] - u[j, i-1])/ (2 * dx))**2 - 2 * ((v[j, i+1] - v[j, i-1])/ (2 * dx) * (u[j+1, i] - u[j-1, i])/ (2 * dy)) - ((v[j+1, i] - v[j-1, i])/ (2 * dy))**2 )

        for l in range(nit + 1):
            pn = p.copy() ##copy the existing values of u into un
            row, col = p.shape
            for j in range(1, row - 1):
                for i in range(1, col - 1):
                    p[j, i] = (((pn[j, i+1] + pn[j, i-1]) * dy**2 + (pn[j+1 ,i] + pn[j-1, i]) * dx**2) / (2 * (dx**2 + dy**2))) - (dx**2 * dy**2 * b[j, i]) / (2 * (dx**2 + dy**2))

            p[:, -1] = p[:, -2] # dp/dx = 0 at x = 2
            p[0, :] = p[1, :]   # dp/dy = 0 at y = 0
            p[:, 0] = p[:, 1]   # dp/dx = 0 at x = 0
            p[-1, :] = 0        # p = 0 at y = 2

        un = u.copy() ##copy the existing values of u into un
        vn = v.copy() ##copy the existing values of v into vn
        row, col = u.shape #simply gets the size of u for loop iterations

        for j in range(1, row - 1):
            for i in range(1, col - 1):
                u[j, i] = un[j, i] - un[j, i] * dt / dx * (un[j, i] - un[j, i-1]) - vn[j, i] * dt / dy * (un[j, i] - un[j-1, i]) - (dt / (rho * 2 * dx)) * (p[j, i+1] - p[j, i-1]) + nu * (dt / dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * (dt / dy**2) * (un[j+1, i] - 2*un[j, i] + un[j+1, i])
                v[j, i] = vn[j, i] - un[j, i] * dt / dx * (vn[j, i] - vn[j, i-1]) - vn[j, i] * dt / dy * (vn[j, i] - vn[j-1, i]) - (dt / (rho * 2 * dy)) * (p[j+1, i] - p[j-1, i]) + nu * (dt / dx**2) * (vn[j, i+1] - 2*vn[j, i] + vn[j, i-1]) + nu * (dt / dy**2) * (vn[j+1, i] - 2*vn[j, i] + vn[j+1, i])

        u[0, :] = 0 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
        u[:, 0] = 0 #sets the whole of [j, 0] to 1 for all j
        u[:, -1] = 0 #sets the whole of [j, last element] to 1 for all j

        v[0, :] = 0 #sets the whole of [0, ] to 1 for all i
        v[-1, :] = 0 #sets the whole of [last element, i] to 1 for all i
        v[:, 0] = 0 #sets the whole of [j, 0] to 1 for all j
        v[:, -1] = 0 #sets the whole of [j, last element] to 1 for all j

@jit(nopython=True)
def NavierChannel(domain, nx, ny, nt, nit, nu, rho, dt, F):
    dx = float(float(domain) / float(nx - 1)) #distance between grid points
    dy = float(float(domain) / float(ny - 1)) #distance between grid points
    p = numpy.zeros((ny, nx)) #sets everything to 0
    b  = numpy.zeros((ny, nx)) #sets everything to 0
    u  = numpy.zeros((ny, nx)) #sets everything to 0
    v  = numpy.zeros((ny, nx)) #sets everything to 0
    un = numpy.zeros((ny, nx)) #sets everything to 0
    vn = numpy.zeros((ny, nx)) #sets everything to 0
    pn = numpy.ones((ny, nx))

    for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
        row, col = b.shape #simply gets the size of u for loop iterations
        for j in range(1, row - 1):
            for i in range(1, col - 1):
                b[j, i] = rho * ( (1 / dt * (((u[j, i+1] - u[j, i-1])/ (2 * dx)) + (v[j+1, i] - v[j-1, i])/ (2 * dy))) - ((u[j, i+1] - u[j, i-1])/ (2 * dx))**2 - 2 * ((v[j, i+1] - v[j, i-1])/ (2 * dx) * (u[j+1, i] - u[j-1, i])/ (2 * dy)) - ((v[j+1, i] - v[j-1, i])/ (2 * dy))**2 )

            b[j, row - 1 ] = rho * ( (1 / dt * (((u[j, 0] - u[j, row-2])/ (2 * dx)) + (v[j+1, row-1] - v[j-1, row-1])/ (2 * dy))) - ((u[j, 0] - u[j, row-2])/ (2 * dx))**2 - 2 * ((v[j, 0] - v[j, row-2])/ (2 * dx) * (u[j+1, row-1] - u[j-1, row-1])/ (2 * dy)) - ((v[j+1, row-1] - v[j-1, row-1])/ (2 * dy))**2 )
            b[j, 0] = rho * ( (1 / dt * (((u[j, 1] - u[j, row-1])/ (2 * dx)) + (v[j+1, 0] - v[j-1, 0])/ (2 * dy))) - ((u[j, 1] - u[j, row-1])/ (2 * dx))**2 - 2 * ((v[j, 1] - v[j, row-1])/ (2 * dx) * (u[j+1, 0] - u[j-1, 0])/ (2 * dy)) - ((v[j+1, 0] - v[j-1, 0])/ (2 * dy))**2 )

        for l in range(nit + 1):
            pn = p.copy() ##copy the existing values of u into un
            row, col = p.shape
            for j in range(1, row - 1):
                for i in range(1, col - 1):
                    p[j, i] = (((pn[j, i+1] + pn[j, i-1]) * dy**2 + (pn[j+1 ,i] + pn[j-1, i]) * dx**2) / (2 * (dx**2 + dy**2))) - (dx**2 * dy**2 * b[j, i]) / (2 * (dx**2 + dy**2))

                p[j, row - 1] = (((pn[j, 0] + pn[j, row-2]) * dy**2 + (pn[j+1 , row -1] + pn[j-1, row -1]) * dx**2) / (2 * (dx**2 + dy**2))) - (dx**2 * dy**2 * b[j, row -1]) / (2 * (dx**2 + dy**2))
                p[j, 0] = (((pn[j, 1] + pn[j, row-1]) * dy**2 + (pn[j+1 , 0] + pn[j-1, 0]) * dx**2) / (2 * (dx**2 + dy**2))) - (dx**2 * dy**2 * b[j, 0]) / (2 * (dx**2 + dy**2))

            p[-1, :] = p[-2, :] # dp/dy = 0 at y = 2
            p[0, :] = p[1, :]   # dp/dy = 0 at y = 0

        un = u.copy() ##copy the existing values of u into un
        vn = v.copy() ##copy the existing values of v into vn
        row, col = u.shape #simply gets the size of u for loop iterations

        for j in range(1, row - 1):
            for i in range(1, col - 1):
                u[j, i] = un[j, i] - un[j, i] * dt / dx * (un[j, i] - un[j, i-1]) - vn[j, i] * dt / dy * (un[j, i] - un[j-1, i]) - (dt / (rho * 2 * dx)) * (p[j, i+1] - p[j, i-1]) + nu * (dt / dx**2) * (un[j, i+1] - 2*un[j, i] + un[j, i-1]) + nu * (dt / dy**2) * (un[j+1, i] - 2*un[j, i] + un[j-1, i]) + F * dt
                v[j, i] = vn[j, i] - un[j, i] * dt / dx * (vn[j, i] - vn[j, i-1]) - vn[j, i] * dt / dy * (vn[j, i] - vn[j-1, i]) - (dt / (rho * 2 * dy)) * (p[j+1, i] - p[j-1, i]) + nu * (dt / dx**2) * (vn[j, i+1] - 2*vn[j, i] + vn[j, i-1]) + nu * (dt / dy**2) * (vn[j+1, i] - 2*vn[j, i] + vn[j-1, i])

            u[j, row - 1] = un[j, row - 1] - un[j, row - 1] * dt / dx * (un[j, row - 1] - un[j, row - 2]) - vn[j, row - 1] * dt / dy * (un[j, row - 1] - un[j-1, row - 1]) - (dt / (rho * 2 * dx)) * (p[j, 0] - p[j, row - 2]) + nu * (dt / dx**2) * (un[j, 0] - 2*un[j, row - 1] + un[j, row - 2]) + nu * (dt / dy**2) * (un[j+1, row - 1] - 2*un[j, row - 1] + un[j-1, row - 1]) + F * dt
            u[j, 0] = un[j, 0] - un[j, 0] * dt / dx * (un[j, 0] - un[j, row - 1]) - vn[j, 0] * dt / dy * (un[j, 0] - un[j-1, 0]) - (dt / (rho * 2 * dx)) * (p[j, 1] - p[j, row - 1]) + nu * (dt / dx**2) * (un[j, 1] - 2*un[j, 0] + un[j, row - 1]) + nu * (dt / dy**2) * (un[j+1, 0] - 2*un[j, 0] + un[j-1, 0]) + F * dt

            v[j, row - 1] = vn[j, row - 1] - un[j, row - 1] * dt / dx * (vn[j, row - 1] - vn[j, row - 2]) - vn[j, row - 1] * dt / dy * (vn[j, row - 1] - vn[j-1, row - 1]) - (dt / (rho * 2 * dx)) * (p[j+1, row -1] - p[j-1, row - 1]) + nu * (dt / dx**2) * (vn[j, 0] - 2*vn[j, row - 1] + vn[j, row - 2]) + nu * (dt / dy**2) * (vn[j+1, row - 1] - 2*vn[j, row - 1] + vn[j-1, row - 1])
            v[j, 0] = vn[j, 0] - un[j, 0] * dt / dx * (vn[j, 0] - vn[j, row - 1]) - vn[j, 0] * dt / dy * (vn[j, 0] - vn[j-1, 0]) - (dt / (rho * 2 * dx)) * (p[j+1, 0] - p[j-1, 0]) + nu * (dt / dx**2) * (vn[j, 1] - 2*vn[j, 0] + vn[j, row - 1]) + nu * (dt / dy**2) * (vn[j+1, 0] - 2*vn[j, 0] + vn[j-1, 0])

        u[0, :] = 0 #sets the whole of [0, ] to 1 for all i
        u[-1, :] = 0 #sets the whole of [last element, i] to 1 for all i
        v[0, :] = 0 #sets the whole of [0, ] to 1 for all i
        v[-1, :] = 0 #sets the whole of [last element, i] to 1 for all i

def main():
    print(
    """
     .d8888b.  888    888 8888888 888b     d888 8888888888 8888888b.         d8888
    d88P  Y88b 888    888   888   8888b   d8888 888        888   Y88b       d88888
    888    888 888    888   888   88888b.d88888 888        888    888      d88P888
    888        8888888888   888   888Y88888P888 8888888    888   d88P     d88P 888
    888        888    888   888   888 Y888P 888 888        8888888P"     d88P  888
    888    888 888    888   888   888  Y8P  888 888        888 T88b     d88P   888
    Y88b  d88P 888    888   888   888   "   888 888        888  T88b   d8888888888
     "Y8888P"  888    888 8888888 888       888 8888888888 888   T88b d88P     888

    """
    )
    print("CHIMERA miniapp: Computational HPC Index for Measuring and Estimating Relative Acceleration - Python Prebuild")
    print("Version 0.1.1 Alpha (Python Version)\n\n")
    print("Note: This is only meant as a demo, and is not the real benchmark. This is made to be ported using CUDA acceleration.\n\n")

    t1 = datetime.now()
    print("Linear Convection Running")
    LinearConvection(2, 61, 61, 0.2, 400, 1)
    LinearConvection(2, 121, 121, 0.2, 800, 1)
    LinearConvection(2, 241, 241, 0.2, 800, 1)
    LinearConvection(2, 241, 241, 0.2, 1600, 2)
    LinearConvection(2, 241, 241, 0.2, 2400, 0.5)
    print("Linear Convection Done\n")

    print("Non-Linear Convection Running")
    NonLinearConvection(2, 51, 51, 0.2, 400)
    NonLinearConvection(2, 101, 101, 0.2, 400)
    NonLinearConvection(2, 151, 151, 0.2, 800)
    NonLinearConvection(2, 201, 201, 0.2, 800)
    print("Non-Linear Convection Done\n")

    print("Heat Equation Running")
    Diffusion(2, 61, 61, 0.1, 400, 0.000111)#copper
    Diffusion(2, 121, 121, 0.25, 800, 0.000111)#copper
    Diffusion(2, 121, 121, 0.25, 800, 0.00001172)#1% carbon steel
    Diffusion(2, 241, 241, 0.5, 1600, 0.0002165)#carbon composite
    Diffusion(2, 241, 241, 0.5, 1600, 0.000019)#air at 300K
    Diffusion(2, 241, 241, 0.5, 1600, 1.43e-7)#water at 300K
    print("Heat Equation Done\n")

    print("Burgers' Equation Running")
    Burgers(3, 41, 41, .000004, 400, 0.00002)#air-water
    Burgers(3, 41, 41, .000004, 800, 0.00002)#air-water
    Burgers(3, 81, 81, .00004, 800, 0.00002)#air-water
    Burgers(3, 81, 81, .00004, 1600, 0.00002)#air-water
    Burgers(3, 161, 161, .0001, 1600, 0.00002)#air-water
    print("Burgers' Equation Done\n")

    print("Laplace Equation Running")
    Laplace(2, 61, 61, 800)
    Laplace(2, 121, 121, 1600)
    Laplace(2, 241, 241, 1600)
    Laplace(2, 241, 241, 2400)
    print("Laplace Equation Done\n")

    print("Poisson Running")
    Poisson(2, 41, 41, 800)
    Poisson(2, 81, 81, 800)
    Poisson(2, 81, 81, 1600)
    Poisson(2, 161, 161, 2400)
    Poisson(2, 241, 241, 2400)
    print("Poisson Done\n")

    print("Navier-Stokes Cavity Running")
    NavierCavity(2, 41, 41, 1200, 50, 1, 1, 0.0001)
    NavierCavity(2, 41, 41, 1200, 50, 1, 2, 0.0001)
    NavierCavity(2, 41, 41, 1200, 50, 1, 0.02, 0.0001)
    NavierCavity(2, 81, 81, 2400, 50, 1, 1, 0.00001)
    NavierCavity(2, 81, 81, 2400, 50, 1, 2, 0.00001)
    print("Navier-Stokes Cavity Done\n")

    print("Navier-Stokes Channel Running")
    NavierChannel(2, 41, 41, 1200, 50, 1, 1, 0.0001, 1)
    NavierChannel(2, 41, 41, 2400, 50, 1, 1, 0.0001, 1)
    NavierChannel(2, 81, 81, 2400, 50, 1, 1, 0.0001, 1)
    NavierChannel(2, 81, 81, 2400, 50, 1, 2, 0.0001, 1)
    print("Navier-Stokes Channel Done\n")
    t2 = datetime.now()

    timediff = t2 - t1

    print("Total time: " + str(timediff.total_seconds() * 1000) + " ms")
    print("SCORE: " + str((30*60*60*60)/(timediff.seconds)) + " CHIMERAmarks (Higher is better)")

if __name__ == '__main__':
    main()
