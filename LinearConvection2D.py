from mpl_toolkits.mplot3d import Axes3D
import numpy
from matplotlib import pyplot, cm
import time, sys
domain = 2 #size
nx = 161 #meshsize in x
ny = 161 #meshsize in y
dx = float(float(domain) / float(nx - 1)) #distance between grid points
dy = float(float(domain) / float(ny - 1)) #distance between grid points

sigma = .2

nt = 200 #number of timesteps
dt = sigma*dx #amount of time each timestep covers - delta t
c = 1 #wavespeed

x = numpy.linspace(0, 2, nx)
y = numpy.linspace(0, 2, ny)

u = numpy.ones((ny, nx)) #sets everything to 1
un = numpy.ones((ny, nx)) #sets everything to 1

print(dx)
u[int(.5 / dy):int(1 / dy + 1), int(.5 / dx):int(1 / dx + 1)] = 2 #sets u = 2 between 0.5 and 1 due to boundry conditions

fig = pyplot.figure(figsize=(11, 7), dpi=100)
ax = fig.gca(projection='3d')
ax.grid(False)
X, Y = numpy.meshgrid(x, y)
surf = ax.plot_surface(X, Y, u[:], linewidth=0)
#pyplot.show()

#fig = pyplot.figure(figsize=(11, 7), dpi=100)
#ax = fig.gca(projection='3d')

for n in range(nt + 1):  #loop for values of n from 0 to nt, so it will run nt times
    un = u.copy() ##copy the existing values of u into un
    row, col = u.shape #simply gets the size of u for loop iterations
    for j in range(1, row):
        for i in range(1, col):
            u[j, i] = un[j, i] - c * dt / dx * (un[j, i] - un[j, i-1]) - c * dt / dy * (un[j, i] - un[j-1, i])
    u[0, :] = 1 #sets the whole of [0, ] to 1 for all i
    u[-1, :] = 1 #sets the whole of [last element, i] to 1 for all i
    u[:, 0] = 1 #sets the whole of [j, 0] to 1 for all j
    u[:, -1] = 1 #sets the whole of [j, last element] to 1 for all j

    surf.remove()
    surf = ax.plot_surface(X, Y, u[:], linewidth=0)
    pyplot.draw()
    pyplot.pause(0.01)
